#################
#  BASIC SETUP  #
#################


#### BASICS ####

FROM ubuntu
MAINTAINER Daniel Taylor
RUN apt-get update
RUN apt-get install -y git


#### NODE ENVIRONMENT ####

RUN curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
RUN apt-get install -y nodejs
RUN apt-get install npm --upgrade
RUN npm install -g express 


#### PYTHON ENVIRONMENT ####

RUN apt-get install python3-pip
RUN pip install
